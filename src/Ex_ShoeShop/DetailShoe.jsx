import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, image, shortDescription, price, id } = this.props.data;
    return (
      <div className="row mt-5 bg-secondary p-4 rounded border border-danger">
        <div className="col-5">
          <img src={image} width="60%" alt="" />
        </div>
        <div className="col-7 text-light text-left ">
          <p>ID: {id}</p>
          <p className="text-bold font-weight-bold">Name: {name}</p>
          <p>Desc: {shortDescription}</p>
          <p>Price: {price}</p>
        </div>
      </div>
    );
  }
}
