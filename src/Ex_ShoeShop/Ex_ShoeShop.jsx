import React, { Component } from "react";
import Cart from "./Cart";
import { DataShoeShop } from "./DataShoeShop";
import DetailShoe from "./DetailShoe";
import ListShoeShop from "./ListShoeShop";
export default class Ex_ShoeShop extends Component {
  state = {
    data: DataShoeShop,
    detail: DataShoeShop[0],
    cart: [],
    number: DataShoeShop,
  };
  handleChangeDetail = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handlePushShoe = (shoe) => {
    let cartItemShoe = { ...shoe, number: 1 };
    let newCart = [...this.state.cart];
    let findIndex = newCart.findIndex((item) => item.id == shoe.id);
    if (findIndex == -1) {
      newCart.push(cartItemShoe);
    } else {
      newCart[findIndex].number += 1;
    }
    this.setState({
      cart: newCart,
    });
  };
  handleup = (shoeid, tanggiam) => {
    let newcart = [...this.state.cart];
    let findIndex = newcart.findIndex((item) => item.id == shoeid);
    if (tanggiam) {
      newcart[findIndex].number += 1;
    } else {
      if (newcart[findIndex].number > 1) {
        newcart[findIndex].number -= 1;
      } else {
        newcart.splice(findIndex, 1);
      }
    }
    this.setState({
      cart: newcart,
    });
  };
  renderListShoe = () => {
    return this.state.data.map((item, index) => {
      return (
        <ListShoeShop
          key={index}
          handleClick={this.handleChangeDetail}
          handlePushShoe={this.handlePushShoe}
          data={item}
        />
      );
    });
  };
  render() {
    return (
      <div className="container">
        <Cart handleup={this.handleup} data={this.state.cart} />
        <div className="row">{this.renderListShoe()}</div>
        <DetailShoe data={this.state.detail} />
      </div>
    );
  }
}
