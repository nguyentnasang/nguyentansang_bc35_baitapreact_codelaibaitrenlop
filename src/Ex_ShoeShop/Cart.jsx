import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.data.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: "50px" }} alt="" />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleup(item.id, true);
              }}
              className="btn btn-success mr-2"
            >
              +
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleup(item.id, false);
              }}
              className="btn btn-success ml-2"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>img</th>
              <th>price</th>
              <th>Quantity: Số Lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
