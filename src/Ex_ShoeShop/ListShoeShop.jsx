import React, { Component } from "react";

export default class ListShoeShop extends Component {
  render() {
    let { image, name, description, price } = this.props.data;
    return (
      <div className="col-4 mt-3">
        <div className="card" style={{ width: "17rem" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              {description.length > 50
                ? description.substr(0, 50) + "..."
                : description}
            </p>
            <h5 className=" text-danger ">{price}$</h5>
            <button
              onClick={() => {
                this.props.handleClick(this.props.data);
              }}
              className="btn btn-primary"
            >
              xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handlePushShoe(this.props.data);
              }}
              className="btn btn-danger"
            >
              thêm vào giỏ
            </button>
          </div>
        </div>
      </div>
    );
  }
}
